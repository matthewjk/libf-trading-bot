
import matplotlib.pyplot as pyplot
import matplotlib.animation as animation
import matplotlib.dates as mdates
import matplotlib.ticker as mtick
import matplotlib
import numpy

import time

pyplot.style.use('ggplot')
matplotlib.rcParams.update({'font.size': 9})

class Graph:
    def __init__(self,symbol):
        self.symbol = symbol
        
        self.fig = pyplot.figure(facecolor='#07000d')
        self.ax1 = pyplot.subplot2grid((6,4), (1,0), rowspan=4, colspan=4, facecolor='#07000d')
        self.ax2 = pyplot.subplot2grid((6,4), (5,0), sharex=self.ax1, rowspan=1, colspan=4, facecolor='#07000d')

        self.timedata = []
        self.libfdata = []
        self.realtimedata = []
        self.discrepancydata = []

        self.setup()

    def setup(self):
        self.fig.canvas.set_window_title('LIBF Bot')
        
        self.ax1.clear()
        self.ax1.grid(True, color='w', linestyle="--")
        self.ax1.yaxis.label.set_color("w")
        self.ax1.spines['bottom'].set_color("#5998ff")
        self.ax1.spines['top'].set_color("#5998ff")
        self.ax1.spines['left'].set_color("#5998ff")
        self.ax1.spines['right'].set_color("#5998ff")
        self.ax1.tick_params(axis='y', colors='w')
        self.ax1.yaxis.label.set_color("w")

        self.ax2.grid(False)
        self.ax2.spines['bottom'].set_color("#5998ff")
        self.ax2.spines['top'].set_color("#5998ff")
        self.ax2.spines['left'].set_color("#5998ff")
        self.ax2.spines['right'].set_color("#5998ff")
        self.ax2.tick_params(axis='x', colors='w')
        self.ax2.tick_params(axis='y', colors='w')
        self.ax2.yaxis.set_major_formatter(mtick.PercentFormatter())
        self.ax2.yaxis.label.set_color("w")
        self.ax2.xaxis.label.set_color("w")

        pyplot.locator_params(axis='x', nbins=10)
        pyplot.figtext(0.50, 0.9, self.symbol, fontsize='large', color='w', ha ='center')
        pyplot.subplots_adjust(left=.09, bottom=.14, right=.94, top=.95, wspace=.20, hspace=0)
        self.fig.canvas.draw()
        pyplot.ion()
        ani = animation.FuncAnimation(self.fig, self.redraw, interval=1000)
        pyplot.show()
        pyplot.draw()
        pyplot.pause(0.05)

    def updatedata(self,l,r,d):
        t = time.strftime("%H:%M",time.localtime(int(time.time())))
        self.timedata.append(t)
        self.libfdata.append(l)
        self.realtimedata.append(r)
        self.discrepancydata.append(d)
        print(self.libfdata)
        # self.redraw()


    def redraw(self,i):

        # ax.get_legend().remove()

        self.ax1.clear()
        self.ax2.clear()

        self.ax1.set_ylabel("Stock price", fontsize=9)
        self.ax2.set_ylabel("Discrepancy", fontsize=9)
        self.ax2.set_xlabel("Time", fontsize=9)

        # self.ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))

        libf, = self.ax1.plot(self.timedata,self.libfdata, color='red', marker='o', linestyle='solid', linewidth=2, markersize=5)
        libf.set_label("LIBF")

        realtime, = self.ax1.plot(self.timedata,self.realtimedata, color='green', marker='o', linestyle='solid', linewidth=2, markersize=5)
        realtime.set_label("Realtime")

        discrepancy, = self.ax2.plot(self.timedata,self.discrepancydata, color='#4ee6fd', marker='o', linestyle='solid', linewidth=2, markersize=5)
        discrepancy.set_label("discrepancy")
        # self.ax2.collections.pop()
        self.ax2.fill_between(discrepancy.get_xdata(), discrepancy.get_ydata(), 0, alpha=0.5, facecolor="#00ffe8", edgecolor="#00ffe8")
        self.ax1.legend(loc=9, ncol=2, prop={'size':7},fancybox=True, borderaxespad=0.)
        
        # pyplot.show()
