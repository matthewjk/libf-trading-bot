import graph

from trade import Trade

import requests
import time

def getlibfsession():
    url = 'https://www.studentinvestor.org/secure/login.php'
    payload = {
        'loginsubmitted':'1',
        'team-name': 'JEM Capital', 
        'team-password': 'Economics99', 
    }
    # 'target':'/secure/login.php', 
    # 'action':'submit'
    s = requests.session()
    s.headers['User-Agent'] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
    r = s.post(url, data=payload)
    # print(r.text)
    return s

def calculateaction(quotedifferencepercent):
    position = 20000
    if(position*quotedifferencepercent>15):
        print("Profitable")
        return True
    elif(position*quotedifferencepercent<=15):
        print("Not Profitable")
        return False
    return False
    


if(__name__=="__main__"):

    session = getlibfsession()

    trades = []
    # trades.append(Trade("TSCO",20000))
    trades.append(Trade("HSBA",20000,session))

    graph.pyplot.show(block=True)

    # Spawn a thread, will sleep for whatver time and check price until it executes the order.
    # Attribute each graph view to trade class thats being watched
