# LIBF Trading Bot
A trading bot exploiting the 15min data delay for the [LIBF Student Investor Challenge](https://www.studentinvestor.org). Changes in the way that the active investor portfolio was managed resulted in this bot never being used.

# Requirements:
- [Python3](https://www.python.org/)
- [Matplotlib](https://matplotlib.org/)
- [Numpy](https://numpy.org/)
- [Beautifulsoup4](https://pypi.org/project/beautifulsoup4/)
- [Requests](https://pypi.org/project/requests/)

# Screenshots:

While the bot is running a graph will be displayed showing quotes from both a real-time price API and the LIBF price data.

![graph1](research/5.png "graph1")
![graph2](research/1.png "graph2")
