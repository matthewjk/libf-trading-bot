import requests
import json
import time
import threading
import datetime

import csv

from bs4 import BeautifulSoup

from graph import Graph

class Trade:
    def __init__(self, symbol,positionsize,session):
        self.symbol = symbol
        self.positionsize = positionsize
        self.session = session
        self.buyprice = 0
        self.buytime = None
        self.sellprice = 0
        self.selltime = None
        # Position is held or not
        self.held = False
        self.libfquote = 0
        self.realtimequote = 0
        self.graph = Graph(symbol)
        # Start automatic data fetching
        self.datafetch()

    # Calculate change in price between realtime quote and libf quote
    def getdiscrepancy(self):
        return ((self.realtimequote-self.libfquote)/self.libfquote)*100

    # Fetch quote from LIBF page
    def getlibfquote(self):
        url = 'https://www.studentinvestor.org/stock-info.php?ticker='+self.symbol+':LN'
        r = self.session.get(url)
        # print(r.text)

        soup = BeautifulSoup(r.text,"html.parser")
        address = soup.find(text="Current Share Price (pence)")
        btag = address.parent
        tdtag = btag.parent
        nexttdtag = tdtag.findNext('td')
        self.libfquote = float(nexttdtag.contents[0])
        print("LIBF quote: "+str(self.libfquote))
        return self.libfquote

    # Fetch quote from realtime API
    def getrealtimequote(self):
        url = "https://www.alphavantage.co/query"
        params = {'function': 'GLOBAL_QUOTE',
        'symbol': 'LON:'+self.symbol,
        'datatype':'json',
        'apikey': 'X6RXLJRPW77ARX8D'}

        r = requests.get(url, params=params)
        # print(r.url)
        print(r.text)
        quotejson = json.loads(r.text)
        self.realtimequote = float(quotejson["Global Quote"]["05. price"])
        print("Realtime quote: "+str(self.realtimequote))
        return self.realtimequote

    def updatedata(self):
        print("Updatedata called")
        ## Use threads to carry out both requets simultaneously
        t1 = threading.Thread(target=self.getlibfquote)
        t2 = threading.Thread(target=self.getrealtimequote)
        t1.start()
        t2.start()
        t1.join()
        t2.join()

        if(self.realtimequote > self.libfquote and self.held == False):
            #Buy
            print("BUYING")
            self.held = True
            self.buytime = datetime.datetime.now().strftime("%H:%M:%S")
            self.buyprice=self.libfquote
        elif(self.realtimequote < self.libfquote and self.held == True):
            # Sell
            print("SELLING")
            self.held = False
            self.selltime = datetime.datetime.now().strftime("%H:%M:%S")
            self.sellprice=self.libfquote
            self.logtrade()

        self.graph.updatedata(self.libfquote,self.realtimequote,self.getdiscrepancy())
        self.datafetch()


    def datafetch(self):
        # Since time is being rounded to nearest 15 and 1 taken away, will start looping 1 minute before
        # Temporary fix
        rounded = datetime.datetime.now() + datetime.timedelta(minutes=1)

        rounded = rounded + (datetime.datetime.min - rounded) % datetime.timedelta(minutes=15)
        rounded = rounded - datetime.timedelta(minutes=1)
        print("Next at: "+str(rounded))

        now = datetime.datetime.now()
        timedifference = (rounded - now).total_seconds()

        print("Next in: "+str(timedifference)+" secs")
        threading.Timer(timedifference,self.updatedata).start()

    def logtrade(self):
        with open('tradelog.csv', mode='a') as tradefile:
            tradewriter = csv.writer(tradefile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tradewriter.writerow([self.symbol,self.buytime,self.buyprice,self.selltime,self.sellprice,((self.sellprice-self.buyprice)/self.buyprice)*100])
            tradefile.close()




    ## Function that will log buy price, potential sell price, trade profiot, time etc
    ## Instead of selling, simply write to csv


    # function to buy itself

    # function to sell 

    # function to log completed trade
